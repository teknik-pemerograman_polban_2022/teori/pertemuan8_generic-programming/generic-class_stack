import java.util.Scanner;

public class App {
    public final static void clearConsole() {
        System.out.print("\033[H\033[2J");
        System.out.flush();
    }

    public final static void pressEnterToContinue() {
        System.out.println("Tekan ENTER untuk melanjutkan...");
        try {
            System.in.read();
        } catch (Exception e) {
        }
    }

    public static void main(String[] args) throws Exception {
        Scanner kScanner = new Scanner(System.in);

        Stack<Integer> StackInteger = new Stack<Integer>();
        Stack<String> StackString = new Stack<String>();

        for (;;) {
            clearConsole();
            System.out.println("Stack bertipe integer\n" + StackInteger.toString());
            System.out.println("Stack bertipe string\n" + StackString.toString());

            System.out.print("Pilih opsi:\n1.Ubah stack integer\n2.ubah stack string\n: ");
            int pilihanStack = kScanner.nextInt();
            System.out.print("\t1.push\n\t2.pop\n\t: ");
            int pilihanAksi = kScanner.nextInt();
            switch (pilihanStack) {
                case 1: {
                    switch (pilihanAksi) {
                        case 1: {
                            System.out.print("\t\tMasukan nilai integer : ");
                            StackInteger.push(kScanner.nextInt());
                            break;
                        }
                        case 2: {
                            StackInteger.pop();
                            break;
                        }
                    }
                    break;
                }
                case 2: {
                    switch (pilihanAksi) {
                        case 1: {
                            System.out.print("\t\tMasukan nilai String : ");
                            StackString.push(kScanner.next());
                            break;
                        }
                        case 2: {
                            StackString.pop();
                            break;
                        }
                    }
                    break;
                }
            }

            pressEnterToContinue();
        }
    }
}
