public class Stack<E> {
    private final int size;
    private int top;
    private E[] elements;

    public Stack(int size) {
        this.size = size > 0 ? size : 100;
        this.top = -1;
        this.elements = (E[]) new Object[this.size];
    }

    public Stack() {
        this(100);
    }

    public boolean isEmpty() {
        return (this.top == -1);
    }

    public boolean isFull() {
        return (this.top == (this.size - 1));
    }

    public void push(E input) {
        if (isFull()) {
            System.out.println("Proses push gagal, stack penuh!");
            return;
        }
        elements[++top] = input;
    }

    public E pop() {
        if (isEmpty()) {
            System.out.println("Proses pop gagal, stack kosong");
            return null;
        }
        return elements[top--];
    }

    public String toString() {
        String result = "";
        result += "==========================\n";
        if (isEmpty()) {
            result += "List kosong..\n";
        } else {
            result += "Isi stack :\n";
            for (int i = 0; i < top + 1; i++) {
                result += elements[i] + " ";
            }
        }
        result += "\n==========================\n";
        return result;
    }
}
